
import { DATA, trainerCardsContainer, renderCard, sortData } from "./index.js"

//////////////////////////////////  ФІЛЬТРАЦІЯ  //////////////////////////////////////

const filterForm = document.querySelector(".sidebar__filters");


// .......................... фільтрація по direction

const filterDataByDirection = (value, data) => {
    let filteredData;

    switch (value) {
        case "all-direction": {
            filteredData = data.filter(elem => true);
            break;
        }
        case "gym": {
            filteredData = data.filter(elem => elem.specialization === "Тренажерний зал");
            break;
        }
        case "fight-club": {
            filteredData = data.filter(elem => elem.specialization === "Бійцівський клуб");
            break;
        }
        case "kids-club": {
            filteredData = data.filter(elem => elem.specialization === "Дитячий клуб");
            break;
        }
        case "swimming-pool": {
            filteredData = data.filter(elem => elem.specialization === "Басейн");
            break;
        }

    }
    return filteredData;

}


// .............................функция фильтр по категории

const filterDataByCategory = (value, data) => {
    let filteredDataByCategory;

    switch (value) {
        case "all-category": {
            filteredDataByCategory = data.filter((elem) => true);
            break;
        }
        case "master": {
            filteredDataByCategory = data.filter(elem => elem.category === "майстер");
            break;
        }
        case "specialist": {
            filteredDataByCategory = data.filter(elem => elem.category === "спеціаліст");
            break;
        }
        case "instructor": {
            filteredDataByCategory = data.filter(elem => elem.category === "інструктор");
            break;
        }
    }
    return filteredDataByCategory;
};


export const filterTrainers = (arr) => {

    const direction = document.querySelector('input[name="direction"]:checked').id;

    const category = document.querySelector('input[name="category"]:checked').id

    console.log(direction)


    const filteredByDirectionData = filterDataByDirection(direction, arr);
    console.log(filteredByDirectionData)

    const filteredData = filterDataByCategory(category, filteredByDirectionData);
    console.log(filteredData)

    trainerCardsContainer.innerHTML = "";

    renderCard(filteredData);

    return filteredData
}


filterForm.addEventListener("submit", (event) => {
    event.preventDefault();

    sortData(filterTrainers(DATA));
});




